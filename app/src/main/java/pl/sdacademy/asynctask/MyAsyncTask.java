package pl.sdacademy.asynctask;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by RENT on 2017-07-25.
 */

public class MyAsyncTask extends AsyncTask<Integer, Integer, Integer> {


    private static final long PERIOD = 5000;
    TextView textView;
    Context context;
    private ProgressDialog pdia;

    public MyAsyncTask(TextView textView, Context context) {
        this.textView = textView;
        this.context = context;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

//        textView.setText("Zaczynamy");

        pdia = new ProgressDialog(context);
        pdia.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pdia.setMax(15);
        pdia.setMessage("Loading...");
        pdia.show();
    }


    @Override
    protected Integer doInBackground(Integer[] params) {
        Integer value = params[0];

        for (; value > -1; value--) {
            publishProgress(value);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        return value;
    }

    @Override
    protected void onProgressUpdate(Integer[] values) {
        super.onProgressUpdate(values);
        pdia.setProgress(values[0]);
        pdia.setMessage("Zostało Ci : " + String.valueOf(values[0]) + " sekund");

//        textView.setText(String.valueOf(values[0]));


    }

    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        textView.setText("skończyłem :D");
        pdia.dismiss();



    }
}
